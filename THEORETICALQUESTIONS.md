1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.

Цикли потрібні для того щоб повторити визначений код необхідну кількість разів, таким чином роблячи код меншим та більш читабельним.
Тобто за допомогою умови можливо визначити скільки разів буде виконано код, який називається тілом цикла.

2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

- У випадку коли завданням було вивести в консоль всі непарні числа, які пербувають у діапазоні від 0 до 300, було використано цикл for, тому що, згідно умови, код потрібно було виконати певну кількість разів з кроком 1 та перевірити кожне значення "і" на парність за допомогою оператора if:
  for (let i = 0; i < 300; i++) {
  if (i % 2 = 0)
  console.log(i);
  }

- У видадку коли однією із умов завдання (ДЗ №2) потрібно було перевірити коректність введених даних, було використано цикл while, тому що необхідно було перевірити дві умови. Цикл while в данному випадку буде повторювати блок коду, поки одна з цих умов буде істинною, тобто поки ім'я є порожнім або вік не є числом.  
   while (!getUserData.name || isNaN(getUserData.age)) {
  getUserData.name = prompt("Bведіть правильно своє ім'я:", !getUserData.name ? "" : getUserData.name);
  getUserData.age = +prompt("Введіть правильно свій вік:", isNaN(getUserData.age) ? "" : getUserData.age);
  }

3. Що таке явне та неявне приведення (перетворення) типів даних у JS?

   Явне перетворення використовується, коли потрібно точно змінити чи визначити тип значення для обробки даних чи виконання певних операцій.  
   Неявне перетворення - це коли автоматично відбувається перетворення типів без явної команди на зміну типу даних.
